/**
 * 
 */
package com.common.distributed.algorithm.blance;

import com.common.distributed.algorithm.model.AlgorithmModel;

/**
 * @author liubing
 *
 */
public interface IBlanceDistributedAlgorithm {
	/**
	 * 获取值
	 * @return
	 */
	public AlgorithmModel getAlgorithmModel();
}
