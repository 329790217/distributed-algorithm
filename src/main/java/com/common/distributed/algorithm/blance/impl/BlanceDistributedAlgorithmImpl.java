/**
 * 
 */
package com.common.distributed.algorithm.blance.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.common.distributed.algorithm.RoundRobinWeight.RoundRobinWeightStrategy;
import com.common.distributed.algorithm.blance.IBlanceDistributedAlgorithm;
import com.common.distributed.algorithm.model.AlgorithmModel;

/**
 * @author liubing
 * 分布式均衡算法
 */
public class BlanceDistributedAlgorithmImpl implements
	IBlanceDistributedAlgorithm {
	
	private  RoundRobinWeightStrategy robinWeightStrategy;
	
	private  Map<String, AlgorithmModel> nodes;
	
	public BlanceDistributedAlgorithmImpl(List<AlgorithmModel> algorithmModels){
		robinWeightStrategy=new RoundRobinWeightStrategy(algorithmModels);
		this.nodes = new HashMap<String, AlgorithmModel>();
		for(int i=0;i<algorithmModels.size();i++){
			this.nodes.put(algorithmModels.get(i).getName(), algorithmModels.get(i));
		}
	}
	/* (non-Javadoc)
	 * @see com.jd.distributed.algorithm.blance.IBlanceDistributedAlgorithm#getAlgorithmModel()
	 */
	public AlgorithmModel getAlgorithmModel() {
		// TODO Auto-generated method stub
		if (nodes.isEmpty()) {
			return null;
		}
		AlgorithmModel algorithmModel=null;
		while(algorithmModel==null){
			String key=robinWeightStrategy.getPartitionIdForTopic();
			algorithmModel=nodes.get(key);
		}
		return algorithmModel;
	}
	
}
